## [3.0.1] - 2022-05-25
### Fixed
- packer 3d

## [3.0] - 2022-05-16
### Added
- New packing algorithm: 3d box

## [2.0] - 2020-01-08
### Changed
- Box interface has 2 new methods
- BoxImplementation implements Box so it had to change

## [1.2] - 2019-12-27
### Added
- new interface - box with unit

## [1.1.3] - 2019-09-25
### Added
- box name in packer separately

## [1.1.2] - 2019-09-17
### Fixed
- Boxes interface

## [1.1.1] - 2019-09-17
### Added
- packages weight calculation
- Boxes interface

## [1.1.0] - 2019-09-13
### Added
- Packer separately

## [1.0.4] - 2019-05-13
### Added
- Weight convert function

## [1.0.3] - 2019-05-13
### Removed
- UPS classes