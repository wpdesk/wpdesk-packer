[![pipeline status](https://gitlab.com/wpdesk/wpdesk-packer/badges/master/pipeline.svg)](https://gitlab.com/wpdesk/wpdesk-packer/pipelines) 
[![coverage report](https://gitlab.com/wpdesk/wpdesk-packer/badges/master/coverage.svg?job=unit+test+lastest+coverage)](https://gitlab.com/wpdesk/wpdesk-packer/commits/master) 
[![Latest Stable Version](https://poser.pugx.org/wpdesk/wpdesk-packer/v/stable)](https://packagist.org/packages/wpdesk/wpdesk-packer) 
[![Total Downloads](https://poser.pugx.org/wpdesk/wpdesk-packer/downloads)](https://packagist.org/packages/wpdesk/wpdesk-packer) 
[![Latest Unstable Version](https://poser.pugx.org/wpdesk/wpdesk-packer/v/unstable)](https://packagist.org/packages/wpdesk/wpdesk-packer) 
[![License](https://poser.pugx.org/wpdesk/wpdesk-packer/license)](https://packagist.org/packages/wpdesk/wpdesk-packer) 

WP Desk Packer
==============
