<?php
/**
 * Boxes interface.
 *
 * @package WPDesk\Packer\BoxFactory
 */

namespace WPDesk\Packer\BoxFactory;

use WPDesk\Packer\Box;

/**
 * Boxes as array.
 */
interface Boxes {

	/**
	 * Get boxes array.
	 *
	 * @return Box[]
	 */
	public function get_boxes();

}