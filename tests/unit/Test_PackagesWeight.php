<?php

use PHPUnit\Framework\TestCase;
use WPDesk\Packer\Box\BoxImplementation;
use WPDesk\Packer\Item\ItemImplementation;
use WPDesk\Packer\PackagesWeight;
use WPDesk\Packer\PackedBox;
use WPDesk\Packer\Packer;

class Test_PackagesWeight extends TestCase {

	/**
	 * Provide data for weight compute test.
	 *
	 * @return array
	 */
	public function items_and_expected_weights() {
		return [
			[
				[ new ItemImplementation( 1, 1, 1, 1 ) ],
				1,
			],
			[
				[
					new ItemImplementation( 1, 1, 1, 1 ),
					new ItemImplementation( 1, 1, 1, 1 ),
				],
				2,
			],
			[
				[
					new ItemImplementation( 1, 1, 1, 0.5 ),
					new ItemImplementation( 1, 1, 1, 0.5 ),
				],
				1,
			],
			[
				[
					new ItemImplementation( 1, 1, 1, 0.5 ),
					new ItemImplementation( 1, 1, 1, 0.5 ),
					new ItemImplementation( 1, 1, 1, 0.5 ),
				],
				1.5,
			],
		];
	}

	/**
	 * Test weight compute.
	 *
	 * @param ItemImplementation[] $items .
	 * @param float                $expected_weight .
	 *
	 * @dataProvider items_and_expected_weights
	 */
	public function test_can_compute_weight( $items, $expected_weight ) {
		$boxpack = new Packer();
		$boxpack->add_box( new BoxImplementation( 100, 100, 10000, 0, 10, uniqid() ) );

		foreach ( $items as $item ) {
			$boxpack->add_item( $item );
		}

		$boxpack->pack();

		$packages = new PackagesWeight( $boxpack->get_packages() );

		$this->assertEquals( $expected_weight, $packages->get_total_weight(), 'Computed weight not equals expected!' );

	}

}
