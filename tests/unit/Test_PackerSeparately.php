<?php

use PHPUnit\Framework\TestCase;
use WPDesk\Packer\Item\ItemImplementation;
use WPDesk\Packer\PackerSeparately;

class Test_PackerSeparately extends TestCase {

	/**
	 * Test pack.
	 * Packs items to packages.
	 *
	 * @param ItemImplementation[] $items .
	 * @param int                  $packages_count .
	 *
	 * @dataProvider items_and_expected_packages
	 */
	public function test_pack( $items, $packages_count ) {
		$packer_separately = new PackerSeparately();

		foreach ( $items as $item ) {
			$packer_separately->add_item( $item );
		}

		$packer_separately->pack();

		$this->assertEquals( $packages_count, count( $packer_separately->get_packages() ) );

		foreach ( $items as $key => $item ) {
			$package = $packer_separately->get_packages()[ $key ];

			$this->assertEquals(
				$item->get_weight(),
				$package->get_packed_weight(),
				'Package weight should equals item weight!'
			);

			$box_internal_data = $package->get_box()->get_internal_data();

			$this->assertEquals( 'Custom box for item', $box_internal_data['box']['name'], 'Box internal data should have name!' );

			$this->assertEquals( $item->get_length(), $box_internal_data['box']['length'], 'Box internal data should have item length!' );

		}

	}

	/**
	 * Data provider for pack test.
	 *
	 * @return array
	 */
	public function items_and_expected_packages() {
		return [
			[
				[
					new ItemImplementation( 10, 20, 30, 40 ),
				],
				1,
			],
			[
				[
					new ItemImplementation( 10, 10, 10, 10 ),
					new ItemImplementation( 10, 10, 10, 10 ),
				],
				2,
			],
		];
	}

}
