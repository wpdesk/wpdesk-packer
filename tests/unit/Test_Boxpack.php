<?php

use PHPUnit\Framework\TestCase;
use WPDesk\Packer\Box\BoxImplementation;
use WPDesk\Packer\Item\ItemImplementation;
use WPDesk\Packer\PackedBox;
use WPDesk\Packer\Packer;

class Test_Boxpack extends TestCase {
	public function square_box_provider() {
		return [
			[ 1, 1, 1, 1 ],
			[ 1, 1, 2, 2 ],
			[ 1, 1, 3, 3 ],
//            [1, 1, 500, 500], // a little long
//            [1, 1, 1000, 1000], // very long
			[ 2, 1, 16, 2 ],
			[ 2, 1, 17, 3 ],

			[ 3, 1, 26, 1 ],
			[ 3, 1, 27, 1 ],
			[ 3, 1, 28, 2 ],
			[ 3, 1, 29, 2 ],

			[ 4, 1, 1, 1 ],
			[ 4, 1, 63, 1 ],
			[ 4, 1, 64, 1 ],
			[ 4, 1, 65, 2 ],
		];
	}

	/**
	 * Only squared boxes. Check if can put square items in square boxes.
	 *
	 * @dataProvider square_box_provider
	 *
	 * @throws Exception
	 */
	public function test_can_square_boxing( $box_square_size, $item_square_size, $item_count, $packages_expected ) {
		$boxpack = new Packer();
		$boxpack->add_box( new BoxImplementation( $box_square_size, $box_square_size, $box_square_size, 0, 0, uniqid() ) );

		for ( $i = 0; $i < $item_count; $i ++ ) {
			$boxpack->add_item( new ItemImplementation( $item_square_size, $item_square_size, $item_square_size ) );
		}
		$boxpack->pack();

		$this->assertEquals( $packages_expected,
			count( $boxpack->get_packages() ),
			"Should pack exactly $packages_expected packages" );

	}

	public function optimized_square_box_provider() {
		return [
			[ 1, 1, 1, '1' ],    // 1 items 1 square = best fit is 1x1x1
			[ 2, 1, 1, '2' ],    // 1 items 2 square = best fit is 2x2x2
			[ 4, 1, 1, '5' ],    // 1 items 4 square = best fit is 5x5x5
			[ 5, 1, 1, '5' ],    // 1 items 5 square = best fit is 5x5x5

			[ 0.5, 8, 1, '1' ],    // 8 items 0.125 square = best fit is 1x1x1
			[ 0.5, 9, 1, '2' ],    // 9 items 0.125 square = best fit is 2x2x2

			[ 1, 8, 1, '2' ],    // 8 items 1 square = best fit is 2x2x2
			[ 1, 9, 1, '5' ],    // 9 items 1 square = best fit is 5x5x5 (other algorithm could get 2x2x2 + 1x1x1)
			[ 1, 125, 1, '5' ],  // 125 items 1 square = best fit is 5x5x5
			[ 1, 126, 2, '5' ],  // 126 items 1 square = best fit is 5x5x5 + 1x1x1 (two packages, exactly one is 5x5x5)
			[ 1, 133, 2, '5' ],  // 133 items 1 square = best fit is 5x5x5 + 2x2x2 (two packages, exactly one is 5x5x5)
		];
	}

	/**
	 * Three boxes - 1, 2, 5 square. Let's check if packing make sense.
	 *
	 * @param float  $item_square_size
	 * @param int    $item_count
	 * @param int    $packages_expected
	 * @param string $package_name_expected_once
	 *
	 * @dataProvider optimized_square_box_provider
	 *
	 * @throws Exception
	 */
	public function test_can_optimize_square_boxing(
		$item_square_size,
		$item_count,
		$packages_expected,
		$package_name_expected_once
	) {
		$boxpack = new Packer();
		$boxpack->add_box( new BoxImplementation( 1, 1, 1, 0, null, '1' ) );
		$boxpack->add_box( new BoxImplementation( 2, 2, 2, 0, null, '2' ) );
		$boxpack->add_box( new BoxImplementation( 5, 5, 5, 0, null, '5' ) );

		for ( $i = 0; $i < $item_count; $i ++ ) {
			$boxpack->add_item( new ItemImplementation( $item_square_size, $item_square_size, $item_square_size ) );
		}
		$boxpack->pack();

		$this->assertEquals( $packages_expected, count( $boxpack->get_packages() ) );
		$expected_found = $this->find_packages_with_id( $boxpack->get_packages(), $package_name_expected_once );
		$this->assertTrue( count( $expected_found ) === 1,
			"Package {$package_name_expected_once} should be exactly once" );
	}

	/**
	 * Finds all packages with a given id
	 *
	 * @param PackedBox[] $packages
	 * @param string      $id
	 *
	 * @return array
	 */
	private function find_packages_with_id( $packages, $id ) {
		return array_filter( $packages,
			function ( PackedBox $package ) use ( $id ) {
				return $id === $package->get_box()->get_unique_id();
			} );
	}

	public function only_possible_solution_with_dimension() {
		return [
			[ 1, 1, 1, 1, '3-box' ],
			// item 1x1x1 = best fit is 3x3x3 as smallest
			[ 3, 2, 3, 1, '3-box' ],
			// item 2x3x2 = best fit is 3x3x3
			[ 20, 1, 1, 1, '1-flat' ],
			// item 20x1x1 = best fit is a flat
			[ 1, 20, 1, 1, '1-flat' ],
			// item 20x1x1 = best fit is a flat
			[ 1, 1, 20, 1, '1-flat' ],
			// item 20x1x1 = best fit is a flat
			[ 20, 1, 2, 1, '2-rectangle' ],
			// item 20x1x2 = can only fit in rectangle

//            [1, 1, 1, 20, '3-box'],          // item 1x1x1 = best fit is flat as it have weight. So weight fit is not used!
		];
	}


	/**
	 * Three exclusive boxes. Check if can find the right one.
	 *
	 * @param $item_width
	 * @param $item_height
	 * @param $item_length
	 * @param $weight
	 * @param $solution
	 *
	 * @dataProvider only_possible_solution_with_dimension
	 *
	 * @throws Exception
	 */
	public function test_can_find_only_possible_solution_with_dimension(
		$item_width,
		$item_height,
		$item_length,
		$weight,
		$solution
	) {
		$boxpack = new Packer();

		$boxpack->add_box( new BoxImplementation( 1, 1, 100, 0, null, '1-flat' ) );
		$boxpack->add_box( new BoxImplementation( 2, 20, 10, 0, null, '2-rectangle' ) );
		$boxpack->add_box( new BoxImplementation( 3, 3, 3, 0, null, '3-box' ) );

		$boxpack->add_item( new ItemImplementation( $item_width, $item_height, $item_length, $weight ) );
		$boxpack->pack();

		$this->assertEquals( 1, count( $boxpack->get_packages() ) );
		$expected_found = $this->find_packages_with_id( $boxpack->get_packages(), $solution );
		$this->assertTrue( count( $expected_found ) === 1, "Package {$solution} should be exactly onece" );
	}

	/**
	 * Can rect when there is no box for the item
	 *
	 * @throws Exception
	 */
	public function test_can_react_to_overfit() {
		$boxpack = new Packer();
		$boxpack->add_box( new BoxImplementation( 1, 1, 1, 0, null,'box-id' ) );
		$boxpack->add_item( new ItemImplementation( 5, 5, 5 ) );
		$boxpack->pack();

		$this->assertTrue( count($boxpack->get_packages()) === 0, "Item should not be packed" );
		$this->assertTrue( count($boxpack->get_items_cannot_pack()) === 1, "Item should be here as unpacked" );
	}
}
